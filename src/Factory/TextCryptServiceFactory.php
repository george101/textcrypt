<?php

declare(strict_types=1);

namespace App\Factory;

use App\Service\CryptService;

/**
 * Class TextCryptServiceFactory
 *
 * Creates instance of CryptService service
 * src/Factory/TextEncryptServiceFactory.php
 */
class TextCryptServiceFactory
{
    public static function createService(): CryptService
    {
        return new CryptService();
    }
}