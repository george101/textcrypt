<?php

declare(strict_types=1);

namespace App\Service;

use Exception;

/**
 * Class CryptService
 *
 * Encrypts and decrypts text with password
 * src/Service/CryptService.php
 */
class CryptService implements CryptServiceInterface
{
    private const CIPHER_METHOD = "AES-256-CBC";
    private const REALLY_VERY_SECRET_IV = "This is my secret iv";

    /**
     * @throws Exception
     */
    public function getEncrypt(string $text, string $password): string
    {

        // hash password
        $key = hash('sha256', $password);

        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', self::REALLY_VERY_SECRET_IV), 0, 16);

        // Encryption of string process starts
        $output = openssl_encrypt($text, self::CIPHER_METHOD, $key, 0, $iv);

        return base64_encode($output);
    }

    /**
     * @throws Exception
     */
    public function getDecrypt(string $ciphertext, string $password): ?string
    {
        $key = hash('sha256', $password);

        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', self::REALLY_VERY_SECRET_IV), 0, 16);

        // Decrypt the string
        $output = openssl_decrypt(base64_decode($ciphertext), self::CIPHER_METHOD, $key, 0, $iv);

        if (!empty($output)) {
            return $output;
        }

        return null;

    }
}