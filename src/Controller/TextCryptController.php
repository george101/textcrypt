<?php

declare(strict_types=1);

namespace App\Controller;

use Exception;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Factory\TextCryptServiceFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class TextEncryptController
 *
 * Contains api routes to display encrypt and decrypt in json and also routes to display templates
 * with encryt and decrypt.
 * src/Controller/TextCryptController.php
 */
class TextCryptController extends AbstractController
{
    private TextCryptServiceFactory $textCryptServiceFactory;
    private SerializerInterface $serializer;

    public function __construct(TextCryptServiceFactory $textCryptServiceFactory,
                                SerializerInterface $serializer)
    {
        $this->textCryptServiceFactory = $textCryptServiceFactory;
        $this->serializer = $serializer;
    }

    /**
     * @throws Exception
     */
    #[Route('/api/encrypt')]
    public function encryptApi(Request $request): Response
    {

        if(!empty($request->query->get('text'))) {
            $text = $request->query->get('text');
        } else {
            $text = "";
        }

        if(!empty($request->query->get('psw'))) {
            $password = $request->query->get('psw');
        } else {
            $password = "";
        }

        $encryptService = $this->textCryptServiceFactory::createService();
        $headers = [];

        $encryptedText = $encryptService->getEncrypt($text, $password);

        // prepares data for view
        $system = array('code' => 200, 'message' => "OK");
        $viewData = array('system' => $system,
            'data' => $encryptedText
        );

        return new Response(
            $this->serializer->serialize($viewData, JsonEncoder::FORMAT),
            200,
            array_merge($headers, ['Content-Type' => 'application/json;charset=UTF-8'])
        );

    }

    #[Route('/encrypt')]
    public function encrypt(): Response
    {
        return $this->render('encrypt.html.twig', [
            'name' => 'Encrypt homepage'
        ]);
    }

    /**
     * @throws Exception
     */
    #[Route('/api/decrypt')]
    public function decryptApi(Request $request): Response
    {

        if(!empty($request->query->get('ciphertext'))) {
            $ciphertext = $request->query->get('ciphertext');
        } else {
            $ciphertext = "";
        }

        if(!empty($request->query->get('psw'))) {
            $password = $request->query->get('psw');
        } else {
            $password = "";
        }

        $cryptService = $this->textCryptServiceFactory::createService();
        $decryptedText = $cryptService->getDecrypt($ciphertext, $password);
        $headers = [];

        if ($decryptedText === null) {
            $message = "Warning: not decrypted";
        } else {
            $message = "OK";
        }

        // prepares data for view
        $system = array('code' => 200, 'message' => $message);
        $viewData = array('system' => $system,
            'data' => $decryptedText
        );

        return new Response(
            $this->serializer->serialize($viewData, JsonEncoder::FORMAT),
            200,
            array_merge($headers, ['Content-Type' => 'application/json;charset=UTF-8'])
        );

    }

    #[Route('/decrypt')]
    public function decrypt(): Response
    {
        return $this->render('decrypt.html.twig', [
            'name' => 'Decrypt homepage'
        ]);
    }

}
