## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)

## General info
Encrypt and Decrypt web app - simple web application in PHP, which allows encrypting and decrypting text.

Backend API - Users communicate with the application over two REST API endpoints. 
The first endpoint is for encrypting text to ciphertext and the second is for decrypting 
ciphertext to the original text.
Main PHP Classes are placed in src/Controller, src/Factory, src/Service.

Frontend - frontend is written as headless application retrieving json data from API endpoints,
jQuery then displays result data in text input selector.
For better reading the javascript with jQuery code was left in header of html template. 
Otherwise, the javascript code would be placed in its own asset file.

## Technologies
Project is created with:
* PHP: 8.1
* Symfony: 6.2
* jQUery: 3.6.3
* Twig 
* HTML

## Setup
To run this project localy, use symfony command for local web server and go to URLs below.

```
$ cd /var/www/html/TextCrypt
$ symfony server:start
```
API endpoint URLs:
http://127.0.0.1:8000/api/encrypt
http://127.0.0.1:8000/api/decrypt

Web page URL:
http://127.0.0.1:8000/encrypt
http://127.0.0.1:8000/decrypt
