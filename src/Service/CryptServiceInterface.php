<?php
declare(strict_types=1);

namespace App\Service;

/**
 * Interface CryptServiceInterface
 */
interface CryptServiceInterface
{
    public function getEncrypt(string $text, string $password): string;

    public function getDecrypt(string $ciphertext, string $password): ?string;

}
